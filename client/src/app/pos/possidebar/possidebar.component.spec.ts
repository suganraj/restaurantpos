import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PossidebarComponent } from './possidebar.component';

describe('PossidebarComponent', () => {
  let component: PossidebarComponent;
  let fixture: ComponentFixture<PossidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PossidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PossidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
