import { Component, OnInit } from '@angular/core';
import { constants } from '../constants';
import { ShareService } from '../providers';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-pos',
  templateUrl: './pos.component.html',
  styleUrls: ['./pos.component.scss']
})
export class PosComponent implements OnInit {

  constructor(
    private shareService: ShareService,
    private titleService: Title,
    private meta: Meta) { }
  userLogin: any;
  logo: any;
  isLogged: any;
  locationDisplay: any;
  locationData: any;
  ngOnInit() {

    // this.shareService.getLoginModal().subscribe(res => {
    //   this.userLogin = res;
    // });

    // this.shareService.getData(constants.getsettings).subscribe(res => {
    //   const appData = res['data'][0];
    //   this.logo = constants.UploadedImage + appData.logo;
    //   this.titleService.setTitle(appData.title);
    //   this.meta.addTag({ name: 'description', content: appData.seo.title });
    //   this.meta.addTag({ name: 'metatag', content: appData.seo.metatag });
    //   this.meta.addTag({ name: 'keywords', content: appData.seo.keyword });
    //   this.meta.addTag({ name: 'description', content: appData.seo.description });
    //   const link = document.querySelector('link[rel*="icon"]') || document.createElement('link');
    //   link['type'] = 'image/x-icon';
    //   link['rel'] = 'shortcut icon';
    //   link['href'] = constants.UploadedImage + appData.favicon;
    //   document.getElementsByTagName('head')[0].appendChild(link);
    // });

  }

}
