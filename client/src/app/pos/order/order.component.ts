import { Component, OnInit } from '@angular/core';
import { ShareService, CartService  } from 'src/app/providers';
import { constants } from 'src/app/constants';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {
  
  categories: any;
  totalRecords: any;
  products: any;
  product: any;
  availtocart: boolean;
  customizableModal: boolean;
  cartitemSub: any;
  cartEmpty: boolean;

  constructor(private shareService: ShareService,
    private messageService: MessageService, private cartService: CartService) { }

  ngOnInit() {
    this.getcategorys();
    this.menuClick();
    this.checkcart();
  }
  private getcategorys() {
    this.shareService.getData(constants.getcategory).subscribe(
      results => {
        this.categories = results['data'];
      },
      err => {
        this.messageService.add({ severity: 'error', summary: 'Error Message', detail: JSON.stringify(err) });
      });
  }
  async menuClick(id?:string){
    let url=id?constants.getproduct+id:constants.getproduct;
    this.shareService.getData(url).subscribe(res => {
      
      this.products = res['data'];
      this.totalRecords = this.products?this.products.length:0;
    }, err => {
      this.messageService.add({ severity: 'error', summary: 'Error Message', detail: JSON.stringify(err) });
    });
  }
  async addCheckoutProduct(product:any){
    this.product = product;
    if (product.variations.length > 0) {
      this.customizableModal = true;
    } else {
      this.addtocart(product);
      this.availtocart = true;
    }
  }

  addtocart(item) {
    this.cartService.saveToCart(item);
  }

  addcustomize({ value }) {
    this.product.selectedattributes = value;
    var totalprice = 0; 

    for (var key in value) {
      if (value.hasOwnProperty(key) && value[key]) {
        value[key].forEach(element => {
          let single = element.split('#');
          totalprice = totalprice + parseInt(single[0]);
        });
      }
    };
    this.product.updatedprice = this.product.price + totalprice;
    this.addtocart(this.product);
    this.availtocart = true;
    this.customizableModal = false; 
  }
  checkcart() {
    this.cartitemSub = this.cartService.getToCart().subscribe(res => {
      
      (res && res.length > 0) ? this.cartEmpty = false : this.cartEmpty = true;
    });
  }

}
