import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PosheaderComponent } from './posheader.component';

describe('PosheaderComponent', () => {
  let component: PosheaderComponent;
  let fixture: ComponentFixture<PosheaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PosheaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PosheaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
