import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderComponent } from './order/order.component';
import { PosRouting } from './pos.routing';
import { PosComponent } from './pos.component';
import { ShareModule } from '../share/share.module';
import { PosheaderComponent } from './posheader/posheader.component';
import { PossidebarComponent } from './possidebar/possidebar.component';
import { CartComponent } from './cart/cart.component';

@NgModule({
  imports: [
    CommonModule,
    ShareModule,
    PosRouting
  ],
  declarations: [
    PosComponent,
    OrderComponent,
    PosheaderComponent,
    PossidebarComponent,
    CartComponent
  ]
})
export class PosModule { }
