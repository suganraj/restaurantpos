import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PosComponent } from './pos.component'; 
import { OrderComponent } from './order/order.component';


const routes: Routes = [
    {
        path: '', component: PosComponent,
        children: [
            { path: '', component: OrderComponent },
            { path: 'order', component: OrderComponent }
        ]
    }
];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})

export class PosRouting { }
