import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { Component, OnInit, Output, EventEmitter, Inject } from '@angular/core';
import { CartService, ShareService } from '../../providers';
import { Globals } from './../../globals';
import { constants } from '../../constants';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  listitems: any = [];
  itemprice: any = 0;
  totalprice: any = 0;
  additionalfare: any;
  coupondisplay: boolean;
  actualitemprice: any;
  coupondata: any;
  disablecoupon: any;
  couponmessage: String;
  @Output() carttotal = new EventEmitter();
  @Output() notes = new EventEmitter();

  constructor(
    @Inject(LOCAL_STORAGE) private localStorage: any,
    private cartService: CartService,
    private shareService: ShareService,
    private globals: Globals) { }

  ngOnInit() {
    this.itemTotal();
  }


  itemTotal() {
    JSON.parse(this.localStorage.getItem('cart-item')) ? this.listitems = JSON.parse(this.localStorage.getItem('cart-item')) : this.listitems = [];
    this.findItemTotal(this.listitems);
    this.cartService.getToCart().subscribe(res => {
      this.listitems = res;
      this.itemprice = 0;
      this.findItemTotal(res);
    });
  }

  findItemTotal(list) {
    list.forEach(function (value, index, arry) {
      if (value.updatedprice) {
        this.itemprice += value.count * value.updatedprice;
      } else {
        this.itemprice += value.count * value.price;
      }
    }, this);
    if (this.coupondata && this.coupondata.couponType === 'percentcart') {
      this.actualitemprice = this.itemprice;
      this.itemprice = this.itemprice - parseInt(((this.itemprice / 100) * this.coupondata.amount).toFixed());
    } else if (this.coupondata && this.coupondata.couponType === 'fixedcart') {
      this.actualitemprice = this.itemprice;
      this.itemprice = this.itemprice - this.coupondata.amount;
    }

    this.getadditionalfare();
  }

  getadditionalfare() {
    this.shareService.getData(constants.getfarerates).subscribe(fare => {
      this.additionalfare = [];
      const that = this;
      fare['data'].forEach(function (item) {
        if (item.faretype === 'percentcart') {
          that.additionalfare = [...that.additionalfare, { fareName: item.fareclasses.title, rate: ((that.itemprice / 100) * item.rate).toFixed() }];
        }
        if (item.faretype === 'fixedcart') {
          that.additionalfare = [...that.additionalfare, { fareName: item.fareclasses.title, rate: item.rate }];
        }
      });
      this.findTotal();
    })
  }

  findTotal() {
    this.totalprice = this.itemprice;
    this.additionalfare.forEach(function (value, index, arry) {
      this.totalprice += parseInt(value.rate);
    }, this);
    // pushing to global
    this.carttotal.emit(this.totalprice);
    this.globals.totalAmount = this.totalprice;
    this.globals.itemCount = this.listitems.length;
  }

  counter(val, id) {
    this.cartService.changeCount(val, id);
  }

  verfiycoupon(coupon) {
    this.shareService.postData(constants.getcoupon + 'verify', { coupon: coupon }).subscribe(res => {
      console.log(res)
      if (res['success']) {
        this.coupondata = res['coupons'][0];
        this.disablecoupon = true;
        this.couponmessage = 'Coupon Added Sucessfully'
        this.itemTotal();
      } else {
        this.couponmessage = 'Coupon Not Found'
      }
    })



  }

  notestrigger(ev) {
    this.notes.emit(ev.target.value)  
  }

}
