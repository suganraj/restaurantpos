import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { Component, OnInit, ViewChild, Inject, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { ShareService, AuthenticationService, CartService, LocationService } from './../../providers';
import { constants } from './../../constants';
import { Globals } from './../../globals';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})

export class CheckoutComponent implements OnInit, OnDestroy {

  private isLogged: Boolean = false;
  private address: any = {};
  private addressList: any;
  private user: any;
  deliveryaddress: any;
  products: any;
  @ViewChild('placesRef.') placesRef: GooglePlaceDirective;
  private payData: any = {};
  private listitems: any;
  private defaultAddress: any;
  note: string;

  constructor(@Inject(LOCAL_STORAGE)
  private localStorage: any,
    private auth: AuthenticationService,
    private cartService: CartService,
    private locationService: LocationService,
    private shareService: ShareService, private globals: Globals, private router: Router) { }

  ngOnInit() {
    this.auth.getloginState('customer').subscribe(res => {
      this.getUserid();
      this.getAddresses();
      this.getDefaultAddress();
      this.isLogged = res;
    });
    this.checkdelivery();
    this.getcartitems();
    document.body.classList.add('checkoutpage');
  }


  private getUserid() {
    this.user = this.auth.getUser('customer');
    if (this.user) {
      this.address.userid = this.user._id;
    }

  }


  public handleAddressChange(address) { 
  }

  checkdelivery() {
    const storedaddress = JSON.parse(this.localStorage.getItem('deliveryaddress'));
    storedaddress ? this.deliveryaddress = storedaddress : this.deliveryaddress = null;
  }

  setDeliveryaddress(address) {
    this.deliveryaddress = address;
    this.localStorage.setItem('deliveryaddress', JSON.stringify(address));
  }

  clearaddress() {
    this.deliveryaddress = null;
    this.localStorage.removeItem('deliveryaddress');
  }
  getAddresses() {
    this.shareService.getData(constants.getAddress + this.address.userid).subscribe(
      results => {
        this.addressList = results['data'];
      },
      err => {
        
        // this.messageService.add({ severity: 'error', summary: 'Error Message', detail: JSON.stringify(err) });
      });
  }

  getDefaultAddress() {
    this.shareService.getData(constants.getDefaultAddress + this.address.userid).subscribe(
      results => {
        this.deliveryaddress = results['data'];
      },
      err => {
        
        // this.messageService.add({ severity: 'error', summary: 'Error Message', detail: JSON.stringify(err) });
      });
  }

  getcartitems() {
    this.products = [];
    var products = [];
    this.cartService.getToCart().subscribe(cartitems => {
      cartitems.forEach(function (item) {
        products = [...products, item._id];
      });

    }); 
  }


  displyaLocationBar() {
    this.locationService.displayLocationsidebar(true, null);

    this.locationService.currentLocationObserver.subscribe(res => {
      this.addressList = [...this.addressList, res];
    })
  }

  tokenFromStripe($event) {
    this.postPayment($event);
  }

  getOrders() {
    JSON.parse(localStorage.getItem('cart-item')) ? this.listitems = JSON.parse(localStorage.getItem('cart-item')) : this.listitems = [];
    this.cartService.getToCart().subscribe(res => {
      this.listitems = res;
    });
  }

  postPayment(result) {
    this.getOrders();
    this.payData.amount = this.globals.totalAmount;
    this.payData.currency = "usd";
    this.payData.stripeToken = result.id;
    this.payData.paymentType = "card";
    this.payData.orderList = JSON.stringify(this.listitems);
    this.payData.userid = this.address.userid;
    this.payData.locationid = this.deliveryaddress._id; 
    this.note ? this.payData.notes = this.note : null;
    this.shareService.postData(constants.makePayment, this.payData).subscribe((data) => {
      this.cartService.clearCart();
      this.router.navigateByUrl('/account/orders');
      // this.messageService.add({ severity: 'success', summary: 'Success Message', detail: 'Address saved successfully' });
    }, (err) => {
      console.error(err);
    });
  }

  onCOD() {
    this.getOrders();
 
    this.payData.amount = this.globals.totalAmount;
    this.payData.currency = "usd";
    this.payData.stripeToken = '';
    this.payData.paymentType = "COD";
    this.payData.orderList = JSON.stringify(this.listitems);
    this.payData.userid = this.address.userid;
    this.payData.locationid = this.deliveryaddress._id;
    this.payData.deliverdate = new Date();
    this.note ? this.payData.notes = this.note : null;  
    this.shareService.postData(constants.createOrder, this.payData).subscribe((data) => {
      this.cartService.clearCart();
      this.router.navigateByUrl('/account/orders');
      // this.messageService.add({ severity: 'success', summary: 'Success Message', detail: 'Address saved successfully' });
    }, (err) => {
      console.error(err);
    });
  }


  notes(data){ 
    this.note = data
  }

  ngOnDestroy(){
    document.body.classList.remove('checkoutpage');

  }

}
