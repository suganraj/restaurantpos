import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpicyComponent } from './spicy.component';

describe('SpicyComponent', () => {
  let component: SpicyComponent;
  let fixture: ComponentFixture<SpicyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpicyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
