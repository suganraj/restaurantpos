import { Component, OnInit, Input, AfterViewInit, ChangeDetectionStrategy } from '@angular/core';
import { ShareService } from './../../providers/share.service';
import { AuthenticationService } from './../../providers/authentication.service';
import { Globals } from './../../globals';
import { Router } from '@angular/router';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  private isLogged: Boolean = false;
  private itemCount: Number = 0;
  @Input() logo: any;

  constructor(
    private shareService: ShareService,
     private auth: AuthenticationService,
     private router: Router,
      private globals: Globals) {

  }

  ngOnInit() {
    this.auth.getloginState('customer').subscribe(res => {
      
      this.isLogged = res;
    });
  }

  ngAfterViewInit() {
    
    this.itemCount = this.globals.itemCount;
  }

  signin() {
    this.shareService.setLoginModal(true);
  }

  signout() {
    this.auth.customerlogout();
    this.router.navigateByUrl('/')

  }

  checkoutpage(items) {
    items > 0 ? this.router.navigateByUrl('/account') : null
  }

}
