import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ShareService, CartService } from './../../providers';
import { constants } from './../../../app/constants';
import { ConfirmationService, MessageService } from 'primeng/api';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit, OnDestroy {

  datasource: any;
  totalRecords: number;
  dishes: any;
  cartEmpty: any;
  cartitemSub: any;
  getdishessub: any;
  listCatasub: any;
  imagePath: any;
  productList: any;
  searchList: any;


  constructor(private http: HttpClient,
    private shareService: ShareService,
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private cartService: CartService
  ) { }

  ngOnInit() {
    this.listCategories();
    this.getdishes();
    this.getProductList();
    this.checkcart();
    this.imagePath = constants.carImage;
    document.body.classList.add('listpage');


    this.cartService.$singleProdCountval.subscribe(res => { 
      this.findProduct(res)
    });

   
  }

  scrollToElement(element): void { 
    document.getElementById(element.replace(/ +/g, "")).scrollIntoView({behavior: "smooth", inline: "nearest"});
  }

  checkcart() {
    this.cartitemSub = this.cartService.getToCart().subscribe(res => {
      
      (res && res.length > 0) ? this.cartEmpty = false : this.cartEmpty = true;
    });
  }
  listCategories() {
    this.listCatasub = this.shareService.getData(constants.listCategories).subscribe(
      results => {
        this.datasource = results['list'];
        this.totalRecords = this.datasource.length;
      },
      err => {
        this.messageService.add({ severity: 'error', summary: 'Error Message', detail: JSON.stringify(err) });
      });
  }

  getdishes() {
    this.getdishessub = this.shareService.getData(constants.getproduct).subscribe(
      results => {
        this.dishes = results['data'];
      },
      err => {
        this.messageService.add({ severity: 'error', summary: 'Error Message', detail: JSON.stringify(err) });
      });
  }

  getProductList() {
    this.shareService.getData(constants.getproductlist).subscribe(
      results => {
        this.productList = results['list'];
      },
      err => {
        this.messageService.add({ severity: 'error', summary: 'Error Message', detail: JSON.stringify(err) });
      });
  }

  onSearchChange(val) {

    this.shareService.getData(constants.searchproducts).subscribe(
      results => {
        console.log("search results");        
        console.log(results)
        this.productList = results['list'];
      },
      err => {
        this.messageService.add({ severity: 'error', summary: 'Error Message', detail: JSON.stringify(err) });
      });

  }

  ngOnDestroy() {
    this.cartitemSub.unsubscribe();
    this.getdishessub.unsubscribe();
    this.listCatasub.unsubscribe();
    document.body.classList.remove('listpage');

  }

  findProduct(item) { 
    let test = this.productList.forEach((cata , i) => {
      cata.subcategories.forEach((product, ind) => { 
        product.products.forEach((element , index) => {
         if(item.id === element._id ){
          this.productList[i].subcategories[ind].products[index]['count'] = item.val;
         } 
        });
      });
    }); 
  }

  trackByFn(index, item) {
    return item.name;
  }

}
