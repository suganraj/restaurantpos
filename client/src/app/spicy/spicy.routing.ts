import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from './../providers';
import { SpicyComponent } from './spicy.component'; 
import { ListComponent } from './list/list.component';
import { CheckoutComponent } from './checkout/checkout.component';


const routes: Routes = [
    {
        path: '', component: SpicyComponent,
        children: [
            { path: '', component: ListComponent },
            { path: 'list', component: ListComponent },
            { path: 'checkout', component: CheckoutComponent }
        ]
    }
];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})

export class SpicyRouting { }
