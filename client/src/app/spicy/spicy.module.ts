import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShareModule } from '../share/share.module';
import { SpicyRouting} from './spicy.routing';
import { SpicyComponent } from './spicy.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component'; 
import { ListComponent } from './list/list.component';
import { CheckoutComponent } from './checkout/checkout.component'; 
import { CartComponent } from './cart/cart.component';
import { LocationbarComponent } from '../share/locationbar/locationbar.component';


@NgModule({
  imports: [
      CommonModule,
      ShareModule,
      SpicyRouting
  ],
  declarations: [
      SpicyComponent,
      HeaderComponent,
      FooterComponent, 
      ListComponent,
      CheckoutComponent, 
      CartComponent
  ]
})

export class SpicyModule { }
