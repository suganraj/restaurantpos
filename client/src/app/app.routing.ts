import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AppComponent} from './app.component';

const routes: Routes = [
 
    {
      path: '',
      loadChildren: './../app/spicy/spicy.module#SpicyModule'
    },
    {
      path: 'admin',
      loadChildren: './../app/admin/admin.module#AdminModule'
    },
    {
      path: 'pos',
      loadChildren: './../app/pos/pos.module#PosModule'
    },
  ];

  @NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
    providers: []
  })
  export class AppRouting {}
