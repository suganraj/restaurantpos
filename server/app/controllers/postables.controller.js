const Postables = require('../models/postables.model.js');


exports.create = (req, res, next) => {

    const postables = new Postables(req.body);
    postables.save((err, data) => {
        if (err) {
            const error = new Error('Some Error in postables');
            return next(error);
        }
        return res.json({ 'success': true, 'message': 'postables added successfully', data });
    })
}

// exports.schema = (req, res, next) => {    
//     var tabledetails= Postables.schema.requiredPaths();
//     // Postables.schema.eachPath(a=>tabledetails.push(a));
//     Postables.findOne().exec((err, data) => {
//         if (err) {
//             const error = new Error('Some Error in postables');
//             return next(error);
//         }
//         return res.json({ 'success': true, 'message': 'postables fetched successfully', 'data': tabledetails });
//     });
// }


exports.findAll = (req, res, next) => {
    Postables.find().exec((err, data) => {
        if (err) {
            const error = new Error('Some Error in postables');
            return next(error);
        }
        return res.json({ 'success': true, 'message': 'postables fetched successfully', 'data': data });
    });
}

exports.findOne = (req, res, next) => {
    Postables.find({ _id: req.params.id }).exec((err, data) => {
        if (err) {
            const error = new Error('Some Error in postables');
            return next(error);
        }
        if (data.length) {
            return res.json({ 'success': true, 'message': 'postables fetched by id successfully', data });
        } else {
            return res.json({ 'success': false, 'message': 'postables with the given id not found' });
        }
    });
};

exports.update = (req, res, next) => {
    Postables.updateOne({ _id: req.params.id }, req.body, { new: true }, (err, data) => {
        if (err) {
            const error = new Error('Some Error in postables');
            return next(error);
        } else {
            return res.json({ 'success': true, 'message': 'postables Updated Successfully', data });
        }
    })
};

exports.delete = (req, res, next) => {
    Postables.findByIdAndRemove(req.params.id, (err, data) => {
        if (err) {
            return res.json({ 'success': false, 'message': 'Some Error' });
        }
        return res.json({ 'success': true, 'message': 'postables Deleted Successfully', data });
    })
};