const mongoose = require('mongoose');

// const postab = require('./fareclasses.model');

var postableSchema = mongoose.Schema({
    tableId: { type: Number, required: true },
    tableName: { type: String, required: true }, 
    orderId: {type: mongoose.Schema.Types.ObjectId, required: true, ref: 'Order'  },   
    seatCount: { type: Number, required: true },    
    isOccupied:  { type: Boolean, required: true },
    active: { type: Boolean, required: true, default: true },
    createdAt: { type: Date, required: true, default: Date.now }
});
 
module.exports = mongoose.model('postable', postableSchema);